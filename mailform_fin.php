<?php

//初期設定しましょう。
require_once('./init.php');

//セッションにそもそもエラーフラグがなければNG
if (!isset($_SESSION['error_flg'])) {
    //入力ページにリダイレクトする
    header("Location:http://liondanceschool.info/mailform_input.php");
    exit;
}
//エラーフラグがfalseならNG
if ($_SESSION['error_flg']) {
    //入力ページにリダイレクトする
    header("Location:http://liondanceschool.info/mailform_input.php");
    exit;
}

// fromから情報を受け取って
foreach ($mailform_elements as $k => $v) {
    $$k = @$_SESSION[$k].'';
}
//（使いおわったので）SESSION変数を初期化させる
//セッションはファイルで残しているので稀にバグを起こすときがあるから最後は消しておく。
$_SESSION = array();

//mailにその情報をおくって
//-日本語でmailを送る下情報
mb_language('japanese');
//これを指定しないとFromの表示名が化ける
mb_internal_encoding("utf-8");
// $fromname = mb_encode_mimeheader("HPより");
// $from = "you.lion.lds@gmail.com";

//headerを設定
$charset = "UTF-8";
$headers['MIME-Version'] = "1.0";
$headers['Content-Type'] = "text/plain; charset=".$charset;
$headers['Content-Transfer-Encoding'] = "8bit";
$headers['Bcc'] = "ke1.10.two6@gmail.com";
// $headers['From'] = '"'.$fromname.'"<'.$from.'>"';
//headerを編集
foreach ($headers as $key => $val) {
    $arrheader[] = $key . ': ' . $val;
}
$strHeader = implode("\n", $arrheader);
//件名を設定（JISに変換したあと、base64エンコードをしてiso-2022-jpを指定する）
$subject = "=?iso-2022-jp?B?". base64_encode(
    mb_convert_encoding("HPからお客様のお問い合わせ", "JIS", "UTF-8")
). "?=";
 
//-mailの送り先とタイトルをざっくり
$to = 'you.lion.lds@gmail.com';


// 本文用の文字列を作って
$message = "
名前：{$name}\r\n
フリガナ:{$name_kana}\r\n
性別：{$sex}\r\n
郵便番号：{$address_numder}\r\n
住所：{$address}\r\n
メールアドレス：{$email}\r\n
電話番号：{$tell}\r\n
お問い合わせ：{$word}\r\n";

$message = wordwrap($message, 70, "\r\n");

$r = mail(
    $to,
    $subject,
    $message,
    $strHeader
);


//『ありがとう』のHTMLを表示する
$smarty_obj->display('mailform_fin.tpl');




ob_end_flush();




