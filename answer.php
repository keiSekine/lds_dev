<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="css/answer.css" rel="stylesheet" type="text/css" />
<link href="css/common.css" rel="stylesheet" type="text/css" />
<meta name="description" content="羽生でキッズダンススクールをお探しならLionDanceSchool！初心者でのダンスを始められてイベントの参加も可能。子供のリズム感を鍛えてカッコいいヒップホップを発表しよう！" />
<script type="text/javascript" src="http://www.google.com/jsapi?key=ABQIAAAAxl-sTT1LAz2ARX4u_ZUqYBTIQ0xWoyFzjDz02KbP-6XbtsHPHxToVX5di4mJXybqn-txmen64kU_3Q"></script>
<script type="text/javascript" src="js/jquery-1.6.1.min.js"></script>
<title>羽生でキッズダンススクールをお探しならLionDanceSchool | Q&A</title>
<script type="text/javascript" src="js/jquery.jrumble.1.3.js">
</script>
<script type="text/javascript" src="js/jquery.jrumble.1.3.min.js">
</script>

<script type="text/javascript">
$(document).ready(function(){
 $('#buru1').jrumble({
  x: 2,
  y: 2,
  rotation: 2,
  speed: 40
 });
  
 $('#buru1').hover(function(){
  $(this).trigger('startRumble');
 }, function(){
  $(this).trigger('stopRumble');
 });
   
});

</script>
<!--アナリティクス-->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-39730488-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>

<body>
<div class="conteiner">
<div class="contents">
  <?php include('./template/header.tpl'); ?>
	<div class="main">

  				<img src="img/answer_main_top.gif" width="764" height="48" alt="ダンス＆スクールに対しての、分からな事や疑問をまとめています" />
         <div class="session_left">
        	<p>スクール選びは、不安や疑問がでてきます。こちらで、さまざまな疑問を載せていますので、すこしでも解消できたら良いなと思います。
					こちらに疑問が無い場合は、お問い合わせください。</p>
         </div>
         <div class="session_right">
          <img src="img/answer_main_image.gif" width="130" height="180" alt="ライオン画像" id="buru1" />
         </div>
	</div><!--main end-->
				<div class="answer_list">
        	<h3>体験、見学をしなくても通う事はできますか？？</h3>
         <p>・基本的に何歳からでも大丈夫です！経験がなくてもゆっくり丁寧にレッスンするので大丈夫です！</p>
        </div>
        
				<div class="answer_list">
        <h3>何歳から学べますか？？または、ダンス経験が無くても大丈夫ですか？？</h3>
         <p>・音のとり方は練習すればどんどん良くなります。基本をしっかりやるので大丈夫です！</p>
        </div>
        
        <div class="answer_list">
         <h3>リズム感、運動神経が無い、身体が固いのですが大丈夫ですか？？</h3>
         <p>・柔軟をレッスンでしっかりやるので、すぐに柔らかくなります！</p>
        </div>
        <div class="answer_list">
         <h3>レッスンについていけるか、心配ですが大丈夫ですか？？</h3>
         <p>・じっくりと進んでいくので問題ないです！</p>
        </div>
        <div class="answer_list">
         <h3>体験、見学をしなくても入会する事はできますか？？</h3>
         <p>・もちろん大丈夫です！一応、体験か見学をすることをおすすめします！</p>
        </div>
        <div class="answer_list">
         <h3>体験、見学に持っていくものは何ですか？</h3>
         <p>・踊りやすい服装、タオル、飲み物があれば問題ないです！</p>
        </div>
        <div class="answer_list">
         <h3>スクールでダンスする時に着ていく服装の指定はありますか？</h3>
         <p>・服装の指定は特にありません！</p>
        </div>
        <div class="answer_list">
         <h3>スタジオには、何分前につけばいいですか？？</h3>
         <p>・余裕をもって5分前には来ていた方が良いと思います！</p>
        </div>
        <div class="answer_list">
         <h3>入会するときに必要なものは何ですか？？</h3>
         <p>・特に必要はないのですが、入会用紙を書いてもらうのと、その月の月謝を頂いてます！</p>
        </div>
        <div class="answer_list">
         <h3>場所が分からないのですがどうすればよいでしょうか？？</h3>
         <p>・場所が変わる場合がありますので、ホームページにその月の場所と地図が載っていますのでそれでわかると思います！</p>
        </div>

<?php include('./template/foother.tpl'); ?>
</div><!--contents end-->
</div>
<script src="js/waza11.js"></script>

<script>
  $(function(){
    mouseStalkerStart('mouseStalker', '<img src="img/mouse.gif" width="30" height="30" />');
  });
</script>
</body>
</html>
