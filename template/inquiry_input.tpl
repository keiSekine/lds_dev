<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="css/inquiry_input.css" rel="stylesheet" type="text/css" />
<link href="css/common.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://www.google.com/jsapi?key=ABQIAAAAxl-sTT1LAz2ARX4u_ZUqYBTIQ0xWoyFzjDz02KbP-6XbtsHPHxToVX5di4mJXybqn-txmen64kU_3Q"></script>
<script type="text/javascript" src="js/jquery-1.6.1.min.js"></script>
<title>LionDanceSchool</title>
</head>

<body>
<div class="conteiner">
<div class="contents">
	<div class="header">
  	<div class="top_left">
			<h2><img src="img/logo.gif" width="94" height="90" alt="LDS" /></h2>
			<h1><img src="img/logo2.gif" width="353" height="78" alt="LionDanceSchool" /></h1>
    </div><!--top_left end-->
  	<div class="top_right">
			<p><a href="#"><img src="img/inquiry_bottom.gif" width="240" height="40" alt="お問い合わせボタン"  /></a></p>
     </div> 
	</div><!--header end-->
  
	<div class="nav">
  	<ul>
    	<li class="top"><a href="/">新着情報</a></li>
      <li class="instructor"><a href="instructor">インストラクター</a></li>
      <li class="tariff_system"><a href="tariff_system">料金システム</a></li>
      <li class="inquiry"><a href="#">お問い合わせアクセス</a></li>
      <li class="answer"><a href="answer">スクールQ＆A</a></li>
    </ul>
	</div><!--nav end-->
  
	<div class="main">

         <p class="maintop">システムの不都合により、サイト上でお問い合わせすることができません。</p>
         <p class="maintop">恐縮でございますが、<a href="tel:08087242208">お電話</a>にてお問い合わせ願いいます。</p>

{include file="foother.tpl"}
<!--contents end-->
</div>
<script src="js/waza11.js"></script>

<script>
  $(function(){
    mouseStalkerStart('mouseStalker', '<img src="img/mouse.gif" width="30" height="30" />');
  });
</script>
</body>
</html>
