<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="css/inquiry_input.css" rel="stylesheet" type="text/css" />
<link href="css/common.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://www.google.com/jsapi?key=ABQIAAAAxl-sTT1LAz2ARX4u_ZUqYBTIQ0xWoyFzjDz02KbP-6XbtsHPHxToVX5di4mJXybqn-txmen64kU_3Q"></script>
<script type="text/javascript" src="js/jquery-1.6.1.min.js"></script>
<title>LionDanceSchool</title>
</head>

<body>
<div class="conteiner">
<div class="contents">
	<div class="header">
  	<div class="top_left">
			<h2><img src="img/logo.gif" width="94" height="90" alt="LDS" /></h2>
			<h1><img src="img/logo2.gif" width="353" height="78" alt="LionDanceSchool" /></h1>
    </div><!--top_left end-->
  	<div class="top_right">
			<p><a href="#"><img src="img/inquiry_bottom.gif" width="240" height="40" alt="お問い合わせボタン"  /></a></p>
     </div> 
	</div><!--header end-->
  
	<div class="nav">
  	<ul>
    	<li class="top"><a href="/">新着情報</a></li>
      <li class="instructor"><a href="instructor">インストラクター</a></li>
      <li class="tariff_system"><a href="tariff_system">料金システム</a></li>
      <li class="inquiry"><a href="#">お問い合わせアクセス</a></li>
      <li class="answer"><a href="answer">スクールQ＆A</a></li>
    </ul>
	</div><!--nav end-->
  
	<div class="main">

         <p class="maintop">当ページ下部に記載の「プライバシーポリシー」 をご確認の上、お客様ご本人が必ず同意された上で入力してください。</p>
         <p class="maintop">下記にご入力の上、“送信”ボタンを押してください。</p>
            
        <div class="inquiry_list">

			<table>
			<tr>
              	<td>名前：</td><td>{$name}</td>
			</tr>
			<tr>
              	<td>フリガナ:</td><td>{$name_kana}</td> 
			</tr>
			<tr>
              	<td>性別：</td>
				<td>{if $sex=="m"}男{/if}
				{if $sex=="f"}女{/if}</td>
			</tr>
			<tr>
              	<td>郵便番号：</td><td>{$address_numder}</td>
			</tr>
			<tr> 
              	<td>住所：<td/><td>{$address}</td>
			</tr>
			<tr> 
             	 <td>メールアドレス：</td><td>{$email}</td>
			</tr> 
			<tr>
              	<td>電話番号：</td><td>{$tell}</td>
			</tr>
			<tr> 
              	<td>お問い合わせ：</td><td>{$word}</td>
			</tr>
			<tr>
				<td><input type="button" value="送信" onClick="location.href='mailform_fin.php'"></td>
				<td><input type="button" value="戻る" onClick="location.href='mailform_input.php'"></td>
			</tr>
			</table>
        </div>
        
        <img src="img/privacy_bar.gif" width="696" height="45" alt="プライバシーポリシー" />
        <p>個人情報の利用目的 ： お問い合わせ内容への回答のために利用いたします。<br />
					取得した個人情報は本人の同意無しに、目的以外では利用しません。<br />
					情報が漏洩しないよう対策を講じ従業員だけでなく委託業者も監督します。<br />
					本人の同意を得ずに第三者に情報を提供しません。本人からの求めに応じ情報を開示します。<br />
					公開された個人情報が事実と異なる場合、訂正や削除に応じます。<br />
					個人情報の取り扱いに関する苦情に対し、適切・迅速に対処します。</p>
	</div><!--main end-->
	
        
		
{include file="foother.tpl"}
</div><!--contents end-->
</div>
<script src="js/waza11.js"></script>

<script>
  $(function(){
    mouseStalkerStart('mouseStalker', '<img src="img/mouse.gif" width="30" height="30" />');
  });
</script>
</body>
</html>
