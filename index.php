<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="css/index.css" rel="stylesheet" type="text/css" />
<link href="css/common.css" rel="stylesheet" type="text/css" />
<meta name="description" content="羽生でキッズダンススクールをお探しならLionDanceSchool！初心者でのダンスを始められてイベントの参加も可能。子供のリズム感を鍛えてカッコいいヒップホップを発表しよう！" />
<script type="text/javascript" src="http://www.google.com/jsapi?key=ABQIAAAAxl-sTT1LAz2ARX4u_ZUqYBTIQ0xWoyFzjDz02KbP-6XbtsHPHxToVX5di4mJXybqn-txmen64kU_3Q"></script>
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript">
//　画像のJS
$(function(){
	$('.fade img:gt(0)').hide();
	setInterval(function(){$('.fade :first-child').fadeOut("slow").next('img').fadeIn("slow").end().appendTo('.fade');}, 5000);
});
</script>
<script type="text/javascript" src="js/jquery.jrumble.1.3.js"></script>
<script type="text/javascript" src="js/jquery.jrumble.1.3.min.js"></script>
<script type="text/javascript">
//ライオウの揺れるJS
$(document).ready(function(){
 $('#buru1').jrumble({
	x: 2,
	y: 2,
	rotation: 2,
	speed: 40
 });
	
 $('#buru1').hover(function(){
	$(this).trigger('startRumble');
 }, function(){
	$(this).trigger('stopRumble');
 });
	 
});
</script>
<title>羽生でキッズダンススクールをお探しならLionDanceSchool | TOP</title>
<!--アナリティクス-->
<script type="text/javascript">
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-39730488-1']);
	_gaq.push(['_trackPageview']);
	(function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
</script>
</head>
<body>
<div class="conteiner">
<div class="contents">
	<?php include('./template/header.tpl'); ?>
	<div id="visual">
		<iframe width="810" height="315" src="https://www.youtube.com/embed/cEQFTZTDqAI?rel=0&controls=0&showinfo=0&autoplay=1&loop=1&muted=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
		<p>※音声が流れます。ご注意をお願致します。</p>
		<!-- <div class="fade">
			<img src="img/main1.jpg" width="800" height="300" alt="main" />
			<img src="img/main2.jpg" width="800" height="300" alt="main2" />
			<img src="img/main3.jpg" width="800" height="300" alt="main3" />
		</div> -->
	</div><!--visual end-->
	<div class="main">
		<p class="bar"><img src="img/top_bar.gif" width="810" height="46" alt="小学生からKIDダンサーを目指すならLION　DANCE　SCHOOL" /></p>
		<div class="session">
			<img	id="buru1" src="img/answer_main_image.gif" width="130" height="180" alt="ライオン" />
			<h6><img src="img/indexmainbox.jpg" width="428" height="120" alt="初心者だってすぐに始められる！！" />
			</h6>
		</div><!--session end-->
		<div class="triangle">
				<p>LDS教室に新しい仲間が増えました。</p>
				<p>詳しくは<a href="/instructor">インストラクターページ</a>をご覧ください。</p>
				<br>
				<p><b style="color: red;">水曜日17:00〜保育園クラス</b>を開講します！(加須スタジオ)</p>
				<p>人数集まり次第なので連絡をお待ちしております。</p>
				<p>分からない事は気軽にお問い合わせください。</p>
		</div>
		<div class="schedule">
			<img src="img/schedule20190917.jpg" alt="スケジュール">
		</div>

		<div class="triangle2">
			<p>保育園クラス：1レッスン45分　月4回　3,000円</p>
			<p>小学生クラス：1レッスン60分　月4回　4,000円</p>
			<p><a href="/tariff_system">料金の詳しい情報はこちら</a></p>
			<p>ライオンダンススクールは、ダンス初心者でもみんなが仲良く楽しむスタジオです。</p>
			<p>何か分からないことがあればメール・電話にてお気軽にお問い合わせください(*´∀｀*)</p>
			<p>Twitterからもお問い合わせできます♫</p>
			<p>KIDSに人気の通称『ライオン先生』と共にかっこいいダンスしませんか。</p>
		</div>
		<div class="triangle">
				<p><b>一緒に働いてくれるインストラクターを募集してます。</b></p>
				<p><b><a href="https://twitter.com/lion_lds" target="_new">Twitter</a>からご連絡下さい。</b></p>
				<p><b>また、直接電話での問い合わせもできます。<a href="tel:08087242208">080-8724-2208</a></b></p>
		</div>
	</div><!--main end-->

	<div id="main_twitter">
		<a class="twitter-timeline" href="https://twitter.com/lion_lds" data-width="100%" data-height="600" data-widget-id="317229837855100929">@lion_lds からのツイート</a>
		<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
				<!--
		 			<p><script type="text/javascript" src="http://widgets.twimg.com/j/2/widget.js"></script>
	<script type="text/javascript">
	new TWTR.Widget({
		version: 2,
		type: 'profile',
		rpp: 15,
		interval: 6000,
		width: 800,
		height: 200,
		theme: {
			shell: {
				background: '#6c5644',
				color: '#ffffff'
			},
			tweets: {
				background: '#ebe0c2',
				color: '#000000',
				links: '#a01b1f'
			}
		},
		features: {
			scrollbar: true,
			loop: false,
			live: true,
			hashtags: true,
			timestamp: true,
			avatars: false,
			behavior: 'all'
		}
	}).render().setUser('lion_lds').start();
	</script></p>
	-->
	</div><!--twitter end -->
	<div class="sponsor_list">
		<h3>●スポンサー</h3>
		<p>Lion Dance Schoolを応援して下さるスポンサー様</p>
		<div>
			<ul>
				<li>
					<a class="example-image-link" href="https://ameblo.jp/sakurazouen2014" data-lightbox="example-set">
						<img class="example-image" src="img/sponsor/sakurazouen.jpg" alt="image-1" />
					</a>
				</li>
			</ul>
		</div>
	</div>

	<?php include('./template/foother.tpl'); ?>
</div><!--contents end-->
</div>

<script src="js/waza11.js"></script>

<script>
	$(function(){
		mouseStalkerStart('mouseStalker', '<img src="img/mouse.gif" width="30" height="30" />');
	});
</script>
</body>
</html>
