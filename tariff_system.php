<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="css/tariff_system.css" rel="stylesheet" type="text/css" />
<link href="css/common.css" rel="stylesheet" type="text/css" />
<meta name="keywords" content="ライオンダンススクール,キッズダンス,羽生,ダンス,キッズ,ヒップホップ," />
<meta http-equiv="description" content="埼玉県羽生市のキッズダンススクールです。わからないことがあればお問い合わせ下さい。羽生で人気のKIDSダンスのスクールといえばLionDanceSchool。初心者でも安心して始められます。体験で参加して楽しくかっこよく踊れるようになりましょう！" />

<script type="text/javascript" src="http://www.google.com/jsapi?key=ABQIAAAAxl-sTT1LAz2ARX4u_ZUqYBTIQ0xWoyFzjDz02KbP-6XbtsHPHxToVX5di4mJXybqn-txmen64kU_3Q"></script>
<title>羽生でキッズダンススクールをお探しならLionDanceSchool | 料金システム</title>
<!--アナリティクス-->
<script type="text/javascript">
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-39730488-1']);
	_gaq.push(['_trackPageview']);
	(function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();

</script>
</head>
<body>
<div class="conteiner">
	<div class="contents">
		<?php include('./template/header.tpl'); ?>
		<div id="visual">
			<div class="fade">
			<img src="img/fee.png" alt="main"  width="100%" />
			</div>
		</div><!--visual end-->
		<div class="main">
			<!-- <div class="triangle">
				<p>【注意】２０１８年４月に料金の変更があります。詳しくはお問い合わせください。</p>
				 入会金5000円の体験、見学を一回500円に変更予定 
			</div> -->

		<!-- <div class="table_style">
			<table>
				<tbody>
					<tr>
						<th>入学金</td>
						<td>3000円</td>
					</tr>
					<tr>
						<th>保育園クラス月４回</td>
						<td>3000円</td>
					</tr>
					<tr>
						<th>小学生クラス月４回</td>
						<td>4000円</td>
					</tr>
				</tbody>
			</table>
		</div> -->

			<div class="main_bottom">
				<p> 体験１回５００円、見学が無料でできます。</p>
				<p> スタジオや活動内容が気になる方はお問い合わせください。</p>
				<p> 業界でもお安いお値段でお得に始められます。</p>
				<p> 多くの方にダンスの魅力を伝えらればと思っております。</p>
				<p> イベントに出れるようフォローしますので、ステージに立ってカッコいいダンサーを目指しましょう！</p>
				<p>※料金の記載がないコースや分からないことは、気軽にお問い合わせください。</p>
			</div>
		</div><!--main end-->
	<?php include('./template/foother.tpl'); ?>
	</div><!--contents end-->
</div>
</body>
</html>
