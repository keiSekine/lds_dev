<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="css/instructor.css" rel="stylesheet" type="text/css" />
<link href="css/common.css" rel="stylesheet" type="text/css" />
<meta name="keywords" content="ライオンダンススクール,キッズダンス,羽生,ダンス,キッズ,ヒップホップ," />
<meta http-equiv="description" content="埼玉県羽生市のキッズダンススクールです。カッコいいダンスを教えています。初心者でも安心！ダンスをやってみたい子！是非、体験も可能なので興味がある熱い子は見学・体験依頼してね！" />
<script type="text/javascript" src="http://www.google.com/jsapi?key=ABQIAAAAxl-sTT1LAz2ARX4u_ZUqYBTIQ0xWoyFzjDz02KbP-6XbtsHPHxToVX5di4mJXybqn-txmen64kU_3Q"></script>
<script type="text/javascript" src="js/jquery-1.6.1.min.js"></script>
<script type="text/javascript">
$(function(){
	$('.fade img:gt(0)').hide();
	setInterval(function(){$('.fade :first-child').fadeOut("slow").next('img').fadeIn("slow").end().appendTo('.fade');}, 5000);
});
</script>
<title>羽生でキッズダンススクールをお探しならLionDanceSchool | インストラクター</title>
<!--アナリティクス-->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-39730488-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

</head>

<body>
<div class="conteiner">
<div class="contents">
	<?php include('./template/header.tpl'); ?>
  
  	<div id="visual">
		<div class="fade">
		<img src="img/main1.jpg" width="800" height="300" alt="top" /><img src="img/main2.jpg" width="800" height="300" alt="top" /><img src="img/main3.jpg" width="800" height="300" alt="top" /></div>
        </div><!--visual end-->


	<div class="main">
		<div class="session">
			<img class="mainimg" src="img/instructor/you.jpg" width="200" height="200" alt="ようすけ画像" />
			<h3>LDSの先生は
				<img src="img/instructor_main_th.gif" width="260" height="32" alt="ライオン先生" />
			</h3>
			<p class="triangle">主に
				<img src="img/instructor_danceimg1.gif" width="18" height="18" alt="ブレイクダンス画像" />
				<em>ブレイクダンス・ＨＩＰ＆ＨＯＰ</em>
				<img src="img/instructor_danceimg2.gif" width="18" height="18" alt="ＨＩＰ＆ＨＯＰ画像" />を得意としていますが、いろいろなダンスに精通しています。</p>
		
			<p class="triangle">ストリートダンスから経験から色々なイベントに参加しています。</p>
			<p class="team">所属チーム
				<img src="img/instructor_sdm.gif" width="70" height="20" alt="SDM" />
			</p>
		
			<p class="triangle">ダウン＆アップのリズムの取り方や身体の基本的な使い方など、細かいところまでしっかり教えるのがうまく、 カッコいい見せ方を意識したパフォーマンスの形をレクチャーしてくれます。
			</p>
		
			<h4>
				<img src="img/instructor_0start.gif" width="390" height="42" alt="ゼロからのスタートでもイベントに参加できるまで、丁寧に学べて楽しくスクールを過ごせます。" />
			</h4>
		
		</div>
		<!--session end-->
<div class="session">
	<img class="mainimg" src="img/instructor/kazue2.png" width="200" height="200" alt="かずえ画像" />
	<h3>LDSの２人目の先生は　かずえ先生</h3>
	<p class="triangle">主に
		<img src="img/instructor_danceimg2.gif" width="18" height="18" alt="ＨＩＰ＆ＨＯＰ画像" />HIP&HOP・GIRLS　HIP＆HOP</em>を得意としています</p>
	<p class="triangle">幼児から大人まで、様々なレッスンの担当ができます。</p>
	<p class="triangle">プロデュース活動もしており、girls team『azure』をプロデュースしてます。</p>
	<p class="triangle">様々なイベント、コンテストなど積極的に活動中です。</p>
	<dl>
		<dt>【受賞履歴】</dt>
		<dd>ジョグメイトカップ 第10回全日本エアロビクスコンテスト 関東大会 3位</dd>
		<dt>【プロデュース】</dt>
		<dd>第4回 全日本小中学生ダンスコンクール 東日本大会 小学生オープン参加の部 銀賞</dd>
		<dd>彩の国 ダンスカップ2017 小学生部門 準優勝</dd>
		<dd>DANCE☆START2017 予選4回戦 6位入賞</dd>
	</dl>
</div>
<!--session end-->
<div class="session">
	<img class="mainimg" src="img/instructor/michako.jpg" width="200" alt="みちゃこ画像" />
	<h3>LDSの3人目の先生は　みちゃこ先生</h3>
	<dl>
		<dt>【2006年】</dt>
		<dd>Darts.Rバックダンサー</dd>
	</dl>
	<dl>
		<dt>【2007年】</dt>
		<dd>HipsLiveダンサー</dd>
	</dl>
	<dl>
		<dt>【2008年】</dt>
		<dd>LAラスベガスダンス留学</dd>
		<dd>USJパークダンサー</dd>
	</dl>
	<dl>
		<dt>【2009年】</dt>
		<dd>V6 PVダンサー</dd>
		<dd>ソフトバンクCM出演</dd>
		<dd>乃木坂46PVダンサー</dd>
		<dd>カラオケMVダンサー</dd>
		<dd>アイドルグループ専属振り付け師</dd>
	</dl>
	<dl>
		<dt>【2015年】</dt>
		<dd>全日本小中学生ダンスコンクール東日本大会振り付け銀賞 受賞</dd>
		<dd>プロポーズフラッシュモブ振り付け</dd>
		<dd>ダンスボーカルユニット《épopue》結成</dd>
	</dl>
</div>
<div class="session">
	<img class="mainimg" src="img/instructor/rena.jpg" width="200" alt="玲奈画像" />
	<h3>LDSの4人目の先生は　玲奈先生</h3>
	<p class="triangle">JAZZ、HIPHOP、LOCK、TAP、THEATER、CONTEMPOPARY、BALLETなど幅広いジャンルのダンスができます。</p>
	<p class="triangle">経験者のスキルアップにはピッタリの先生となります</p>
	<dl>
		<dt>【20016年】</dt>
		<dd>日本ダンス部選手大会「DANCE STADIUM」（主催：ストリートダンス協会・日経新聞）にゲスト出演</dd>
	</dl>
	<dl>
		<dt>【2017年】</dt>
		<dd>スポーツ庁後援「日本ダンス大会」エキシビションにゲスト出演</dd>
		<dd>「乃木坂46」夏の全国ツアーFINALオープニングバックダンサー</dd>
	</dl>
</div>
<div class="session">
	<img class="mainimg" src="img/instructor/moa.png" width="200" alt="望愛画像" />
	<h3>LDSの5人目の先生は　望愛（モア）先生</h3>
	<p class="triangle">オールジャンル踊りこなすことができます。</p>
	<p class="triangle">目力の強さを活かし、強く美しく踊ります。</p>
	<dl>
		<dt>【20017年】</dt>
		<dd>第５回「日本ダンス大会」にゲストダンサーとして出演</dd>
		<dd>「乃木坂46」真夏の全国ツアー2019FINAL(東京ドーム)バックダンサー出演</dd>
	</dl>
	<dl>
		<dt>【2018年】</dt>
		<dd>第５回「日本ダンス大会」にゲストダンサーとして出演</dd>
		<dd>「17Live　web CM」野村翔一郎バックダンサー出演</dd>
		<dd>「第60回輝く！レコード大賞」氷川きよしバックダンサー</dd>
	</dl>
</div>
<div class="session">
	<img class="mainimg" src="img/instructor/kaz.png" width="200" alt="Kaz画像" />
	<h3>LDSの6人目の先生は　Kaz(カズ)先生</h3>

 日本散打博撃協会日本代表選手団元主将 [主戦歴] 【国内】 第9回拳杯 虎タイガー級 優勝 第１０回全日本格斗打撃選手権 リアルプロテクト 重量級 優勝 ・民國九十五年獅子盃第二屆全國散打搏撃錦標賽
75kg以下級 優勝 ・民國九十五年第二屆散打搏撃冠軍争覇賽 2006第二屆台湾散打搏撃拳王争覇賽 男子組 中量級 榮獲 争覇優勝

	<p class="triangle">散打の先生です。</p>
	<p class="triangle">海外でも数多くの賞を受賞と、75キロ以下級世界１の実績があります。</p>
	<p class="triangle">散打に興味がある方はご連絡ください。</p>
	<dl>
		<dt>【役職・地位】</dt>
		<dd>全日本闘技空手道連盟常任理事</dd>
		<dd>日本散打博撃協会日本代表選手団元主将</dd>
	</dl>
	<dl>
		<dt>【国内】</dt>
		<dd>第9回拳杯 虎タイガー級 優勝 </dd>
		<dd>第１０回全日本格斗打撃選手権 リアルプロテクト 重量級 優勝</dd>
	</dl>
	<dl>
		<dt>【海外】</dt>
		<dd>民國九十五年獅子盃第二屆全國散打搏撃錦標賽 75kg以下級 優勝</dd>
		<dd>民國九十五年第二屆散打搏撃冠軍争覇賽 2006第二屆台湾散打搏撃拳王争覇賽 男子組 中量級 榮獲 争覇優勝</dd>
	</dl>
</div>
<!--session end-->
	</div><!--main end-->
<?php include('./template/foother.tpl'); ?>
</div><!--contents end-->
</div>
<script src="js/waza11.js"></script>

<script>
  $(function(){
    mouseStalkerStart('mouseStalker', '<img src="img/mouse.gif" width="30" height="30" />');
  });
</script>
</body>
</html>
