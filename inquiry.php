<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link href="css/inquiry.css" rel="stylesheet" type="text/css" />
	<link href="css/common.css" rel="stylesheet" type="text/css" />
	<meta name="keywords" content="ライオンダンススクール,キッズダンス,羽生,ダンス,キッズ,ヒップホップ," />
	<meta http-equiv="description" content="埼玉県羽生市のキッズダンススクールです。カッコいいダンスを教えています。初心者でも安心！安心価格で始めやすいスクールです！月額設定なので子供のライフに合わせやすい！見学が一回無料なので様子もわかり安心して任せられます。親しみやすい先生で楽しく学べます。" />
	<script type="text/javascript" src="http://www.google.com/jsapi?key=ABQIAAAAxl-sTT1LAz2ARX4u_ZUqYBTIQ0xWoyFzjDz02KbP-6XbtsHPHxToVX5di4mJXybqn-txmen64kU_3Q"></script>
	<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
	<link rel="stylesheet" href="css/lightbox.min.css">
	<title>羽生でキッズダンススクールをお探しならLionDanceSchool | お問い合わせ</title>
	<!--アナリティクス-->
	<script type="text/javascript">
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-39730488-1']);
		_gaq.push(['_trackPageview']);

		(function() {
			var ga = document.createElement('script');
			ga.type = 'text/javascript';
			ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(ga, s);
		})();
	</script>

</head>

<body>
	<div class="conteiner">
		<div class="contents">
			<?php include('./template/header.tpl'); ?>
			<div class="main">
				<div class="inquiry_list">
					<div class="map">
						<iframe width="300" height="200" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.co.jp/maps?f=q&amp;source=s_q&amp;hl=ja&amp;geocode=&amp;q=%E3%80%92348-0034+%E5%9F%BC%E7%8E%89%E7%9C%8C%E7%BE%BD%E7%94%9F%E5%B8%82%E4%B8%8B%E5%B7%9D%E5%B4%8E%EF%BC%91+shell&amp;aq=&amp;sll=36.136419,139.565592&amp;sspn=0.043393,0.066004&amp;brcurrent=3,0x601f328c8b93e2af:0x4cb5a7e0bac88d85,0&amp;ie=UTF8&amp;hq=shell&amp;hnear=%E5%9F%BC%E7%8E%89%E7%9C%8C%E7%BE%BD%E7%94%9F%E5%B8%82%E4%B8%8B%E5%B7%9D%E5%B4%8E&amp;t=m&amp;fll=36.138204,139.556429&amp;fspn=0.010848,0.016501&amp;st=116167581741289814193&amp;rq=1&amp;ev=zi&amp;split=1&amp;ll=36.137996,139.557115&amp;spn=0.010848,0.016501&amp;output=embed"></iframe><br /><small><a href="https://maps.google.co.jp/maps?f=q&amp;source=embed&amp;hl=ja&amp;geocode=&amp;q=%E3%80%92348-0034+%E5%9F%BC%E7%8E%89%E7%9C%8C%E7%BE%BD%E7%94%9F%E5%B8%82%E4%B8%8B%E5%B7%9D%E5%B4%8E%EF%BC%91+shell&amp;aq=&amp;sll=36.136419,139.565592&amp;sspn=0.043393,0.066004&amp;brcurrent=3,0x601f328c8b93e2af:0x4cb5a7e0bac88d85,0&amp;ie=UTF8&amp;hq=shell&amp;hnear=%E5%9F%BC%E7%8E%89%E7%9C%8C%E7%BE%BD%E7%94%9F%E5%B8%82%E4%B8%8B%E5%B7%9D%E5%B4%8E&amp;t=m&amp;fll=36.138204,139.556429&amp;fspn=0.010848,0.016501&amp;st=116167581741289814193&amp;rq=1&amp;ev=zi&amp;split=1&amp;ll=36.137996,139.557115&amp;spn=0.010848,0.016501" style="color:#0000FF;text-align:left">大きな地図で見る</a></iframe>
					</div>
					<p class="span"><span>◆羽生スタジオ（M'sスタジオ）</span></p>
					<p>加須市街からお越しの方は国道１２５号でむさしの村の手前の信号【志多見】を右折します。<br />
						羽生市街からお越しの方は国道１２２号でマクドナルドを通過します。</p>
					<p>*【shell】に隣接するスタジオが当スタジオになります。*</p>
					<p>＊場所に変更がある場合はTWITTERにてお知らせがあります＊</p>
				</div>
				<div class="inquiry_list">
					<div class="map">
						<iframe width="300" height="200" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1611.3602406360076!2d139.595275!3d36.124667!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6018ccb15c526995%3A0xb208c4c3034ca80b!2z44CSMzQ3LTAwNTUg5Z-8546J55yM5Yqg6aCI5biC5Lit5aSu77yR5LiB55uu77yY4oiS77yT77yRIOODjuODvOODluODq-ODgOODs-OCueOCueOCv-OCuOOCqg!5e0!3m2!1sja!2sjp!4v1547790425278"></iframe><br /><small><a href="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1611.3602406360076!2d139.595275!3d36.124667!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6018ccb15c526995%3A0xb208c4c3034ca80b!2z44CSMzQ3LTAwNTUg5Z-8546J55yM5Yqg6aCI5biC5Lit5aSu77yR5LiB55uu77yY4oiS77yT77yRIOODjuODvOODluODq-ODgOODs-OCueOCueOCv-OCuOOCqg!5e0!3m2!1sja!2sjp!4v1547790425278" style="color:#0000FF;text-align:left">大きな地図で見る</a></iframe>
					</div>
					<p class="span"><span>◆加須スタジオ（旧ノーブルダンススタジオ）</span></p>
					<p>加須駅北口を出てコスメショップ店（ビューティショップむかわ）の横の通りを曲がり進みます。<br>
						突き当たりを右に曲がります。</p>
					<p>*ラーメン屋（十勝ラーメン）を左に曲がったすぐにあります。*</p>
					<p>＊場所に変更がある場合はTWITTERにてお知らせがあります＊</p>
				</div>
				<div class="inquiry_list01">
					<p>分からないこと、始めたいけど、どうすればいいか分からない方、<br />
						気軽にお問い合わせてください！</p>
					<p>体験・見学も承りますのでお問い合わせ下さい。<br />
						見学より体験することがダンサーへの大きな一歩となります。</p>
					<p>問い合わせ先（電話番号）：<a href="tel:08087242208">08087242208</a></p>
					<p>LINEでも問い合わせできます。↓</p>
						<div class="">
							<span style="margin-left: 20px;">
								<script type="text/javascript" src="//media.line.me/js/line-button.js?v=20140411"></script>
								<script type="text/javascript">
									new media_line_me.LineButton({
										"pc": true,
										"lang": "ja",
										"type": "d",
										"text": "LionDanceSchoolは誰でもうまくなれるキッズスクール",
										"withUrl": true
									});
								</script>
							</span>
						</div>
				</div>
			</div>
			<div class="inquiry_list">
				<h3>●GALLERY</h3>
				<a class="example-image-link" href="img/gallery/2017_old1.jpg" data-lightbox="example-set"><img class="example-image" src="img/gallery/2017_old1.jpg" alt="image-1" /></a>
				<a class="example-image-link" href="img/gallery/2017_old2.jpg" data-lightbox="example-set"><img class="example-image" src="img/gallery/2017_old2.jpg" alt="image-2" /></a>
				<a class="example-image-link" href="img/gallery/2017_old3.jpg" data-lightbox="example-set"><img class="example-image" src="img/gallery/2017_old3.jpg" alt="image-3" /></a>
			</div>
		</div>
		<!--main end-->
		<?php include('./template/foother.tpl'); ?>
	</div>
	<!--contents end-->
	</div>
	<script src="js/lightbox.js"></script>

</body>

</html>